<?php

function enqueue_style(){
    wp_register_style("reset", STYLE_DIR . "/reset.css", [], "1.0.0", false );
    wp_register_style("style", get_stylesheet_uri(), [], "1.0.0", false );
    wp_register_style("bellotatext","https://fonts.googleapis.com/css2?family=Bellota+Text:wght@300&display=swap");
    wp_register_style("header", STYLE_DIR . "/header.css" , [], "1.0.0", false );
    wp_register_style("footer", STYLE_DIR . "/footer.css" , [], "1.0.0", false );
    wp_register_style("myaccount", STYLE_DIR . "/my-account.css" , [], "1.0.0", false );
    wp_register_style("singleproduct", STYLE_DIR . "/single-product.css" , [], "1.0.0", false );
    wp_register_style("archiveproduct", STYLE_DIR . "/archiveproduct.css" , [], "1.0.0", false );
    wp_register_style("checkout", STYLE_DIR . "/checkout.css" , [], "1.0.0", false );
    wp_register_script("checkoutclick", JAVA_DIR . "/bruna.js", [], "1.0.0", true);
    wp_register_script("checkoutclick2", JAVA_DIR . "/bruna2.js", [], "1.0.0", true);


    wp_enqueue_style( "reset");
    wp_enqueue_style( "bellotatext");
    wp_enqueue_style( "style");
    wp_enqueue_style( "header");
    wp_enqueue_style( "footer");
    wp_enqueue_style( "myaccount");
    wp_enqueue_style( "singleproduct");
    wp_enqueue_style( "archiveproduct");
    wp_enqueue_style( "checkout");
    wp_enqueue_script( "checkoutclick");
    wp_enqueue_script( "checkoutclick2");

}

?>