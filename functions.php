<?php

//Variáveis
define("ROOT_DIR", get_theme_file_path());
define("STYLE_DIR", get_template_directory_uri() . "/assets/css");
define("IMAGES_DIR", get_template_directory_uri() . "/assets/images");
define("JAVA_DIR", get_template_directory_uri() . "/assets/js");
define("INCLUDES_DIR", ROOT_DIR . "/includes");

//Includes
include_once(INCLUDES_DIR . "/enqueue.php");
include_once(INCLUDES_DIR . "/setup-theme.php");
include_once(INCLUDES_DIR . "/format-product.php");
include_once(INCLUDES_DIR . "/form_field_address.php");


//Ganchos
add_action("wp_enqueue_scripts","enqueue_style");
add_action("after_setup_theme", "setup_theme");

add_action("loop_shop_per_page","comidas_loop_shop_per_page");
function comidas_loop_shop_per_page(){
    return 8;
}


add_filter("woocommerce_before_checkout_form_cart_notices","__return_false");
add_filter("wc_add_to_cart_message_html","__return_false");
add_filter("woocommerce_cart_item_removed_notice_type","__return_false");
add_filter("woocommerce_enable_order_notes_field","__return_false");

// Nossa função hooked - $fields é passada através do filtro!

?>
<?php
    add_filter("woocommerce_checkout_fields","custom_override_checkout_fields");
    function custom_override_checkout_fields($fields){
        $fields = [];
        $fields["billing"]["billing_email"] = [
            "label" => "Email para contato",
            'placeholder'=> "Digite seu email",
            "required" => true,
            "class" => "billing_email"
        ];
        $fields["billing"]["billing_first_name"] = [
            "label" => "Nome",
            'placeholder'=> " Digite seu nome",
            "required" => true,
            "class" => "billing_name"
        ];
        $fields["billing"]["billing_last_name"] = [
            "label" => "Sobrenome",
            'placeholder'=> "Digite seu sobrenome",
            "required" => true,
            "class" => "billing_last_name"
        ];
        $fields["billing"]["billing_phone"] = [
            "label" => "Telefone fixo",
            'placeholder'=> "(21) XXXXX-XXXX",
            "required" => true,
            "class" => "billing_phone"
        ];
        $fields["billing"]["billing_celphone"] = [
            "label" => "Celular",
            'placeholder'=> "(21) XXXXX-XXXX",
            "required" => true,
            "class" => "billing_phone2"
        ];
        $fields["billing"]["billing_postcode"] = [
            "label" => "CEP",
            'placeholder'=> "XXXXX-XXX",
            "required" => true,
            "class" => "billing_postcode"
        ];
        $fields["billing"]["billing_address_1"] = [
            "label" => "Logradouro",
            'placeholder'=> "Rua Lorem Ipsum, 150, Pro",
            "required" => true,
            "class" => "billing_address_1"
        ];
        $fields["billing"]["billing_address_2"] = [
            "label" => "Complemento",
            'placeholder'=> "Bl 2, Apto 905",
            "required" => true,
            "class" => "billing_address_2"
        ];
        $fields["billing"]["billing_address_2"] = [
            "label" => "Bairro",
            'placeholder'=> "Lorem Ipsum",
            "required" => true,
            "class" => "billing_address_22"
        ];
        $fields["billing"]["billing_city"] = [
            "label" => "Cidade",
            'placeholder'=> "Lorem Ipsum",
            "required" => true,
            "class" => "billing_city"
        ];
        return $fields;
    }
?>
