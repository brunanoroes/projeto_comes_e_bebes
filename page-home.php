<?php
get_header();
?>

<section class="top">
    <h1 class="tituloHome">Comes&Bebes</h1>
    <p class="sloganHome"> O restaurante para todas as fomes</p>
</section>

<section class="conteudo">
    <h2 class="subtConteudo">CONHEÇA A NOSSA LOJA</h2>
    
    <p class="subtCategorias">Tipos de pratos principais</p>
    <div class="doDia">
    <p class="subtDoDia">Pratos do dia de hoje:</p>   
    </div>
</section>

<section class="bottom">
  <h2 class="subtConteudo">VISITE NOSSA LOJA FÍSICA</h2>
  <div class="containerFisica">
      <div class="visite">

      <div class="gmap_canvas">
      <iframe width="345" height="203" id="gmap_canvas" src="https://maps.google.com/maps?q=<?php echo "Niterói" ?>&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
      </div>

      <div class="endereco">
      </div>

      </div>

      <div class="slideshow">

        <div class="slides" id="slide1">
          <img src="<?php echo IMAGES_DIR.loja_fisica.jpg; ?>" style="width:100%">
        </div>

        <div class="slides" id="slide2">
          <img src="<?php echo IMAGES_DIR.loja_fisica(2).jpg; ?>" style="width:100%">
        </div>

        <div class="slides" id="slide3">
          <img src="<?php echo IMAGES_DIR.loja_fisica(3).jpg; ?>" style="width:100%">
      </div>
  </div>
</section>



  <style>
    .slides{
        display:none
    }
    .numtext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    .top{
      background-color:#7A247D;
      display:flex;
      flex: column;
      align-content:center;
      justify-content: center;
    }

    .bottom{
      background-color:#7A247D;
      
    }
    .displayFisica{
      display:flex;
      flex-direction:row;
    }
    .visite{
      display: flex;
      flex-direction: column:
    }

  </style>

  <script>
    function showSlides(n) {
        let i;
        let slides = document.getElementsByClassName("slides");

        if (n > slides.length) {
            slideIndex = 1
            }
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slides[slideIndex-1].style.display = "block";
}

    let slideIndex = 1;
    showSlides(slideIndex);
  </script>
</section>


<?php
get_footer();
?>