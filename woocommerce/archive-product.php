<?php
get_header();
?>
<div class="tudo">
	<section id="oi">
		<h1 class="h1archive">SELECIONE UMA CATEGORIAS</h1>
		<section class="categorias">
			<div id="divnordestina" class="block">
				<img src="<?php echo IMAGES_DIR."/nordestina.webp"; ?>" alt="">
				<a href="/product-category/noderstina/" id="nordestina" class="text34">NORDESTINA</a>
			</div>
			<div id="divvegana" class="block">
				<img src="<?php echo IMAGES_DIR."/Vegana.webp"; ?>" alt="">
				<a href="/product-category/vegana/" id="vegana" class="text34">VEGANA</a>
			</div>
			<div id="divmassas" class="block">
				<img src="<?php echo IMAGES_DIR."/Massas.jpg"; ?>" alt="">
				<a href="/product-category/massas/" id="massas" class="text34">MASSAS</a>
			</div>
			<div id="divjaponesa" class="block">
				<img src="<?php echo IMAGES_DIR."/Japonesa.jpg"; ?>" alt="">
				<a href="/product-category/japonesa/" id="japonesa" class="text34">JAPONESA</a>
			</div>
		</section>
	</section>
	<section id="pratos">
		<h1 class="h1archive">PRATOS</h1>
		<?php
		 $category = get_queried_object();
		$category2 = $category->name; 
		if (is_product_category()) {
			?>
			<p id="pdaarchive" >Comida <?php echo $category2 ?></p>
		<?php
		}
		?>
		<div id="buscar">
			<p>Buscar por nome:</p>
			<form action="<?php bloginfo("url"); ?>/shop/" method="get">
				<input type="text" name="s" id="s">
			</form>
		</div>
		<div id="caixa">
			<div id="ordenar">
				<label class="labelarchive" >Ordenar por:</label>
				<div class="inputbruna3"> <?php woocommerce_catalog_ordering(); ?></div>
			</div>
			<div id="filtro">
				<p>Filtro de preço:</p>
				<div id=brunass>
					<div id="de">
						<label class="labelbruna" for="input">De:</label>
						<input id="inputbruna2" type="number">
					</div>
					<div id="ate">
						<label class="labelbruna" for="input">Até:</label>
						<input id="inputbruna3" type="number">
					</div>
				</div>
		</div>
	</section>
	<?php
	?>
	<div class="foraloop">
	<?php	
	if(have_posts()){
		while(have_posts()){
			the_post();
			$id = get_the_ID();
			formatar_produto(wc_get_product($id));
			// print_r(wc_get_product($id));
		}
	}

	?>
	</div>
	<?php
	echo get_the_posts_pagination();
	?>
</div>
<br>
<?php
get_footer();

?>


