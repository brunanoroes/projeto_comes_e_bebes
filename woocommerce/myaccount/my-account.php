<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); ?>

<style>
        .MyAccount-content {
            flex-direction: column;
            margin: auto;
            margin-left: 15rem;
    margin-right: 15rem;
        }

		.MyAccount-content a{
    
text-decoration: none;
    color: #000;
    
}
        
</style>

<div class="MyAccount-content">
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_content' );

		$URL_ATUAL= "$_SERVER[REQUEST_URI]";

	if( $URL_ATUAL == '/' . 'my-account'.'/' ){
   		wc_get_template( 'myaccount/form-edit-account.php' ); 
	} 
	?>
</div>
