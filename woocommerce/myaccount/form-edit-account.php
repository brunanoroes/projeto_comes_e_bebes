<?php


defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>

<style>

.input-text {
	
    border: 0px ;
    box-shadow: 0 0 0 0;
    outline: 0;
	border-bottom: 2px solid #6A066D;
	font-family: 'Bellota Text';
font-style: normal;
font-weight: 300;
font-size: 18px;
line-height: 23px;
background: #E7FAFE;
color: #000000;
}

body p{
	font-family: 'Bellota Text';
font-style: normal;
font-weight: 400;
font-size: 24px;
line-height: 30px;
/* identical to box height */


color: #000000;


}

body{
	background: #E7FAFE;
}


.button-save-account{
	background-color: #6A066D;
	color: white;
	border: 0px;
	font-size: 24px;
	text-align: center;
	width: 289px;
	height: 41px;
	
}

.div_button{
	display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
}
</style>


<form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">

		<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;</label>
		
		<input type="text" class="input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" placeholder="Digite seu nome" />
		
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;</label>
		<input type="text" class="input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" placeholder="Digite seu sobrenome" />
	</p>
	<div class="clear"></div>


	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email">E-mail</label>
		<input type="email" class="input-text" name="account_email" id="account_email" autocomplete="email" placeholder="Digite seu email" />
	</p>

	<fieldset>
		<legend><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="input-text" name="password_current" id="password_current" placeholder="Digite sua senha atual" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<input type="password" class="input-text" name="password_1" id="password_1" autocomplete="off" placeholder="Digite seu Digite sua nova senha" />
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
			<input type="password" class="input-text" name="password_2" id="password_2" autocomplete="off" placeholder="Confirme sua nova senha"  />
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>
<div class="div_button">
	<p>
		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="button-save-account" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>
</div>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
