<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<style>
	.MyAccount-navigation {
    width: 100%;
    height: 5rem;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #F5C661;
    text-align: center;
    
}
.MyAccount-navigation a{
    font-family: 'Bellota Text';
font-style: normal;
font-weight: 700;
font-size: 24px;
line-height: 30px;
text-decoration: none;
    color: #000;
    margin-left: 6rem;
    margin-right: 6rem;
    text-align: center;
}
</style>

<nav class="MyAccount-navigation">
	<ul class="MyAccount-navigation">
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) {  ?>
			<li>
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php  if( esc_html( $label ) == '') {echo "Endereço";}else { echo esc_html( $label );} ?></a>
                
			</li>
		<?php }  ?>
	</ul>
</nav>
<br>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
