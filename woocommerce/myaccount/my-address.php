<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.6.0
 */

defined( 'ABSPATH' ) || exit;

$customer_id = get_current_user_id();


if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing'  => __( 'Billing address', 'woocommerce' ),
		),
		$customer_id
	);
} else {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing' => __( 'Billing address', 'woocommerce' ),
		),
		
	); 
}

$oldcol = 1;
$col    = 1;
?>

<style>
	.top-description{
		justify-content: space-between;
		font-family: 'Bellota Text';
		font-style: normal;
		font-weight: 700;
		font-size: 24px;
		line-height: 30px;
		text-align: justify;
		width: 70%;
		color: #000000;
		flex-direction: row;
		display:flex
	}
	.top-description a{
		font-family: 'Bellota Text';
		font-style: normal;
		font-weight: 700;
		font-size: 24px;
		line-height: 30px;
		text-align: justify;
		text-decoration-line: underline;

		color: #09A7B1;
	}
	td {
		width: 25rem;
		font-family: 'Bellota Text';
		font-style: normal;
		font-weight: 400;
		font-size: 18px;
		line-height: 23px;
		

		color: #000000;
	}
	table {
		width:100%
	}
	.editar {
		text-align: right;
	}

	.titulo-nome-tel{
		font-family: 'Bellota Text';
		font-style: normal;
		font-weight: 400;
		font-size: 24px;
		line-height: 30px;
		text-align: justify;

		color: #6A066D;
	}

	
</style>

<p>
	<?php echo apply_filters( 'woocommerce_my_account_my_address_description', esc_html__( 'The following addresses will be used on the checkout page by default.', 'woocommerce' ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
</p>

<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) : ?>
	<div class="u-columns woocommerce-Addresses col2-set addresses">
<?php endif; ?>

<?php foreach ( $get_addresses as $name => $address_title ) : ?>
	<?php
		$address = wc_get_account_formatted_address( $name );
		$col     = $col * -1;
		$oldcol  = $oldcol * -1;

		
	?>
	<br>
	<div class="u-column<?php echo $col < 0 ? 1 : 2; ?> col-<?php echo $oldcol < 0 ? 1 : 2; ?> woocommerce-Address">
		<div class="top-description">
			<h3>Endereço de Entrega</h3>
			<a href="<?php echo esc_url( wc_get_endpoint_url( 'add-address', $name ) ); ?>" class="editar">Adicionar Endereço</a>
		</div>
		<br>
		<div class="top-description">
		<address>
			<table>
				<tr>
					<td class="titulo-nome-tel">Nome</td>
					<td class="titulo-nome-tel">22222-222</td>
					<td>
					<?php
				echo $address ? wp_kses_post( $address ) : esc_html_e( 'You have not set up this type of address yet.', 'woocommerce' );
			?>
					</td>
					<td class="editar">
					<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', $name ) ); ?>" class="edit"><?php echo $address ? esc_html__( 'Edit', 'woocommerce' ) : esc_html__( 'Add', 'woocommerce' ); ?></a>
					</td>
				</tr>
			</table>
			<!-- <?php
				echo $address ? wp_kses_post( $address ) : esc_html_e( 'You have not set up this type of address yet.', 'woocommerce' );
			?>
			<a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', $name ) ); ?>" class="edit"><?php echo $address ? esc_html__( 'Edit', 'woocommerce' ) : esc_html__( 'Add', 'woocommerce' ); ?></a> -->
		</address>
		</div>
	</div>
	

<?php endforeach; 

?>

<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) : ?>
	</div>
	<?php
endif;

?>