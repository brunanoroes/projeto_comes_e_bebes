<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */

defined( 'ABSPATH' ) || exit;

$page_title = ( 'billing' === $load_address ) ? esc_html__( 'Billing address', 'woocommerce' ) : esc_html__( 'Shipping address', 'woocommerce' );

do_action( 'woocommerce_before_edit_account_address_form' ); 
$endereco = 
	 [ 
	'billing_first_name' =>  [ 'label' => 'Nome', 'required' => true, 'class' =>  [ 0 => 'form-row-first',  ], 'autocomplete' => 'given-name', 'priority' => 10, 'value' => '',  ], 
	 'billing_last_name' =>  [ 'label' => 'Sobrenome', 'required' => true, 'class' =>  [ 0 => 'form-row-last',  ], 'autocomplete' => 'family-name', 'priority' => 20, 'value' => '',  ], 
	 'billing_postcode' =>  [ 'label' => 'CEP', 'required' => true, 'class' =>  [ 0 => 'form-row-wide', 1 => 'address-field',  ], 'validate' =>  [ 0 => 'postcode',  ], 'autocomplete' => 'postal-code', 'priority' => 90, 'value' => '',  ], 
	 'billing_address_1' =>  [ 'label' => 'Logradouro', 'placeholder' => 'Nome da rua e número da casa', 'required' => true, 'class' =>  [ 0 => 'form-row-wide', 1 => 'address-field',  ], 'autocomplete' => 'address-line1', 'priority' => 50, 'value' => '',  ], 
	 'billing_address_2' =>  [ 'label' => 'Complemento', 'placeholder' => '', 'required' => true, 'class' =>  [ 0 => 'form-row-wide', 1 => 'address-field',  ], 'autocomplete' => 'address-line1', 'priority' => 50, 'value' => '',  ], 
	 'billing_city' =>  [ 'label' => 'Cidade', 'required' => true, 'class' =>  [ 0 => 'form-row-first',  ], 'autocomplete' => 'given-name', 'priority' => 10, 'value' => '',  ], 
	 'billing_neighborhood' =>  [ 'label' => 'Bairro', 'required' => true, 'class' =>  [ 0 => 'form-row-last',  ], 'autocomplete' => 'family-name', 'priority' => 20, 'value' => '',  ], 
	 
];
//print_r($endereco);
?>

<style>

.input-text {
	
    border: 0px ;
    box-shadow: 0 0 0 0;
    outline: 0;
	border-bottom: 2px solid #6A066D;
	font-family: 'Bellota Text';
font-style: normal;
font-weight: 300;
font-size: 18px;
line-height: 23px;
background: #E7FAFE;
color: #000000;
}

body p{
	font-family: 'Bellota Text';
font-style: normal;
font-weight: 400;
font-size: 24px;
line-height: 30px;
/* identical to box height */


color: #000000;


}

.title-title{
	font-family: 'Bellota Text';
font-style: normal;
font-weight: 400;
font-size: 24px;
line-height: 30px;
/* identical to box height */

font-weight: 800;
color: #000000;


}

body{
	background: #E7FAFE;
}


.button-save{
	background-color: #6A066D;
	color: white;
	border: 0px;
	font-size: 24px;
	text-align: center;
	width: 289px;
	height: 41px;
	
	
}

.div_button{
	
	flex-direction: row;
	justify-content: center;
	align-items: center;
	text-align: center;

}
</style>

<?php if ( ! $load_address ) : ?>
	<?php wc_get_template( 'myaccount/my-address.php' ); ?>
<?php else : ?>

	<form method="post">
		<p>Os endereços a seguir serão usados na página de finalizar pedido como endereços padrões, mas é possível modificá-los durante a finalização do pedido</p>
		<br>
		<p class ="title-title">Novo endereço</p><?php // @codingStandardsIgnoreLine ?>

		<div class="woocommerce-address-fields">
			<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

			<div class="woocommerce-address-fields__field-wrapper">
				<?php
				foreach ( $endereco as $key => $field ) {
					woocommerce_form_field_address( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
				}
				?>
			</div>
				<br>
			<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>
                
		</div>
		
			<p class="div_button">
				<button type="submit" class="button-save" name="save_address" value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>"><?php esc_html_e( 'Save address', 'woocommerce' ); ?></button>
				<?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
				<input type="hidden" name="action" value="edit_address" />
			</p>
        
	</form>

<?php endif; 
//var_export($address)
?>

<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>
