<section class="tudinho">
	<br>
	<br>
	<section class="secaosp"> <?php

	if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
		return;
	}

	global $product;

	$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
	$post_thumbnail_id = $product->get_image_id();
	$wrapper_classes   = apply_filters(
		'woocommerce_single_product_image_gallery_classes',
		array(
			'woocommerce-product-gallery',
			'woocommerce-product-gallery--' . ( $post_thumbnail_id ? 'with-images' : 'without-images' ),
			'woocommerce-product-gallery--columns-' . absint( $columns ),
			'images',
		)
	);
	?>
	<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
		<figure class="figura">
			<?php
			if ( $post_thumbnail_id ) {
				$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
			} else {
				$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
				$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
				$html .= '</div>';
			}

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped

			do_action( 'woocommerce_product_thumbnails' );
			?>
		</figure>
	</div>

		<section class="secaosp2">

		<?php


		if ( ! defined( 'ABSPATH' ) ) {
			exit; // Exit if accessed directly.
		}

		the_title( '<h1 class="product_title entry-title">', '</h1>' );


		defined( 'ABSPATH' ) || exit;

		global $post;

		$heading = apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) );

		?>

		<?php the_content(); ?>

		<?php

		defined( 'ABSPATH' ) || exit;

		global $product;

		if ( ! $product->is_purchasable() ) {
			return;
		}

		echo wc_get_stock_html( $product ); // WPCS: XSS ok.

		if ( $product->is_in_stock() ) : ?>

			<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

			<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
				<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>



				<?php
				do_action( 'woocommerce_before_add_to_cart_quantity' );

				woocommerce_quantity_input(
					array(
						'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
						'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
						'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
					)
				);
	?>
			<div class="botao-preco">
	<?php
				global $product;

				?>
				<p class="<?php echo esc_attr( apply_filters( 'woocommerce_product_price_class', 'price420' ) ); ?>"><?php echo $product->get_price_html(); ?></p>
				<?php 
				do_action( 'woocommerce_after_add_to_cart_quantity' );
				?>

				<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt<?php echo esc_attr( wc_wp_theme_get_element_class_name( 'button' ) ? ' ' . wc_wp_theme_get_element_class_name( 'button' ) : '' ); ?>">ADICIONAR</button>

				<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
				</form>

				<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

				<?php endif; ?>
			</div>
		</section>
	</section>

	<?php
	
			$id = get_the_ID();
			$singleproduto = wc_get_product($id);
			$iddacategoria = $singleproduto->get_category_ids()[0];
			if ($iddacategoria == 16){
				$brunalinda = "NORDESTINA";
			}
			if ($iddacategoria == 17){
				$brunalinda = "VEGANA";
			}
			if ($iddacategoria == 19){
				$brunalinda = "MASSAS";
			}
			if ($iddacategoria == 18){
				$brunalinda = "JAPONESA";
			}
			


			?>
			<p id="psp" >MAIS COMIDA <?php echo $brunalinda ; ?></p>
			

<?php

function formatar_produto2($product)
{
	?>
		<div class="card-prato">
			<div class="img"><?= $product->get_image(); ?></div>
			<div class="oi3">
				<h4 id="nomeproduto"><?= $product->get_name(); ?></h4>
				<div class="oi2">
					<p id="preco"><?= $product->get_price(); ?></p>
					<button class="vixi"><a  href="/?attachment_id=<?= $product->get_id(); ?>"><img src="<?php echo IMAGES_DIR."/carrinho2.png"; ?>" alt="foto de carrinho"></a></button>
				</div>
			</div>
		</div>
	<?php

}

?>
<div class="foraloop2">

<?php

	$lista=[];

	if ($iddacategoria == 16){
		$nordestina = [110,109,107,106,105,104,103,102,101,99];
		for($i=0;$i<=4;$i++){
			if($nordestina[$i] != $id){
				formatar_produto2(wc_get_product($nordestina[$i]));
			}
		}
	}
	elseif ($iddacategoria == 17){
		$vegana = [98,97,96,95,94,93,92,91,90,89];
		for($i=0;$i<=4;$i++){
			if($vegana[$i] != $id){
				formatar_produto2(wc_get_product($vegana[$i]));
			}
		}
	}
	elseif ($iddacategoria == 19){
		$massas = [120,119,118,117,116,115,114,113,112,111];
		for($i=0;$i<=4;$i++){
			if($massas[$i] != $id){
				formatar_produto2(wc_get_product($massas[$i]));
			}
		}
	}
	elseif ($iddacategoria == 18){
		$japonesa = [88,86,85,84,83,82,81,80,79,21];
		for($i=0;$i<=4;$i++){
			if($japonesa[$i] != $id){
				formatar_produto2(wc_get_product($japonesa[$i]));
			}
		}
	}
	?>
</div>
<br>
<br>

