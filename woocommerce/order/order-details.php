<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.6.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>

<style>
    .details__title {
        font-family: 'Bellota Text';
        font-style: normal;
        font-weight: 700;
        font-size: 32px;
        line-height: 40px;
        display: flex;
        align-items: center;

        color: #000000;
    }
    .title-itens{
        font-family: 'Bellota Text';
        font-style: normal;
        font-weight: 700;
        font-size: 24px;
        line-height: 30px;
        
        align-items: left;

        color: #000000;
    }

	table {
		width:  60rem;
	}
	th, td,tr {
			text-align: left;
			padding: 3px;
			align-items: flex-start;
			display: flex;
	
	


		}

	.td-body{
		font-family: 'Bellota Text';
		font-style: normal;
		font-weight: 400;
		font-size: 18px;
		line-height: 23px;
		/* identical to box height */

		display: flex;
		align-items: center;

		color: #000000;
	}

	.woocommerce-order-detailss{
		flex-direction: row;
		display:flex;
		width: 50%
		
	}
	
</style>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>
    <br>
	<h2 class="details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>
    <br>
	<div class="woocommerce-order-detailss">
	<table class="">

		<thead>
			<tr>
				<th class="title-itens"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
			</tr>
			
			
		</thead>
		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );

			foreach ( $order_items as $item_id => $item ) {
				$product = $item->get_product();

				wc_get_template(
					'order/order-details-item.php',
					array(
						'order'              => $order,
						'item_id'            => $item_id,
						'item'               => $item,
						'show_purchase_note' => $show_purchase_note,
						'purchase_note'      => $product ? $product->get_purchase_note() : '',
						'product'            => $product,
					)
				);
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
			?>
		</tbody>

		 <!-- <tfoot>
			<?php
			foreach ( $order-> get_order_item_totals() as $key => $total ) {
				?>
					<tr>
						<th scope="row" class="title-itens"><?php echo esc_html( $total['label'] ); ?></th>
						
						
					</tr>
					<tr><td><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td></tr>
					<?php 
			}
			?> 
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					
				</tr>
				<tr><td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td></tr>
			<?php endif; ?>
		</tfoot> -->
	</table> 
	<!-- <?php
	
	$order_items = $order->get_order_item_totals();
	 foreach ($order_items as $items_key => $items_value) {  
		if($items_value['label'] == 'Método de pagamento:'){
		echo $items_value['label']; //this works
		}
			   
	   }
	?>

	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?> -->

	<table class="">

		<thead>
			<tr>
				<th class="title-itens">
					<?php
					$order_items_total = $order->get_order_item_totals();
					foreach ($order_items_total as $items_key => $items_value) {  
					   if($items_value['label'] == 'Método de pagamento:'){
						$items_value['label'] = 'Método de pagamento';
					   echo $items_value['label']; //this works
					   }
							  
					  }
					?>
				</th>
			</tr>
			<tr>
				<td class="td-body">
					  <?php
					  $order_items_total = $order->get_order_item_totals();
					  foreach ($order_items_total as $items_key => $items_value) {  
						 if($items_value['label'] == 'Método de pagamento:'){
						 echo $items_value['value']; //this works
						 }
								
						}
					  ?>
					  </td>
			</tr>
			
			
		</thead>
			
					
					</table>
					</div>
					<div class="woocommerce-order-detailss">
					<table class="">

		<thead>
			<tr>
				<th class="title-itens">
					<?php
					$order_items_total = $order->get_order_item_totals();
					foreach ($order_items_total as $items_key => $items_value) {  
					   if($items_value['label'] == 'Subtotal:'){
						$items_value['label'] = 'Subtotal';
					   echo $items_value['label']; //this works
					   }
							  
					  }
					?>
				</th>
				
			</tr>
			<tr>
				<td class="td-body">
					  <?php
					  $order_items_total = $order->get_order_item_totals();
					  foreach ($order_items_total as $items_key => $items_value) {  
						 if($items_value['label'] == 'Subtotal:'){
						 echo $items_value['value']; //this works
						 }
								
						}
					  ?>
					  
					  </td>
			</tr>
			
			
		</thead>
					</table>
					<div class="woocommerce-order-detailss">
					<table class="">

		<thead>
			<tr>
				
				<th class="title-itens">
					<?php
					$order_items_total = $order->get_order_item_totals();
					foreach ($order_items_total as $items_key => $items_value) {  
					   if($items_value['label'] == 'Total:'){
						$items_value['label'] = 'Total';
					   echo $items_value['label']; //this works
					   }
							  
					  }
					?>
				</th>
			</tr>
			<tr>
				<td class="td-body" >
					  
					  <?php
					  $order_items_total = $order->get_order_item_totals();
					  foreach ($order_items_total as $items_key => $items_value) {  
						 if($items_value['label'] == 'Total:'){
						 echo $items_value['value']; //this works
						 }
								
						}
					  ?>
					  </td>
			</tr>
			
			
		</thead>
					</table>
					</div>
</section>

<?php
$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing'  => __( 'Billing address', 'woocommerce' ),
			'shipping' => __( 'Shipping address', 'woocommerce' ),
		),
		$customer_id
	);
} else {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing' => __( 'Billing address', 'woocommerce' ),
		),
		$customer_id
	);
}

$oldcol = 1;
$col    = 1;
?>



<?php foreach ( $get_addresses as $name => $address_title ) : ?>
	<?php
		$address = wc_get_account_formatted_address( $name );
		$col     = $col * -1;
		$oldcol  = $oldcol * -1;
	?>

	
		<table>
		<thead>
		<tr>
			<th class="title-itens">Endereço</th>
		</tr>
			<tr>
			<td class="td-body">
				<?php
				echo $address ? wp_kses_post( $address ) : esc_html_e( 'You have not set up this type of address yet.', 'woocommerce' );
			?>
			</td>
			</tr>
</thead>
			</table>
		
	

<?php endforeach; ?>

<?php if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) : ?>
	<?php
endif;
?>
<?php
/**
 * Action hook fired after the order details.
 *
 * @since 4.4.0
 * @param WC_Order $order Order data.
 */
do_action( 'woocommerce_after_order_details', $order );

if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
