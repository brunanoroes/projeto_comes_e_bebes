<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php bloginfo("name");?> / <?php the_title(); ?> </title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="body">
<header>
    <div id="block1">
        <a id="paginicial" href="/home"><?php the_custom_logo(); ?></a>
        <div id="block12">
            <img src="<?php echo IMAGES_DIR."/lupa.png"; ?>" alt="foto de lupa">
            <form action="<?php bloginfo("url"); ?>/shop/" method="get">
				<input type="text" name="s" id="s1" placeholder="Sashimi">
			</form>
        </div>
    </div>
    <div id="block2">
        <a href="/shop" id="fazer_pedido">Faça um pedido</a>
        <button id="abrir-carrinho"><img src="<?php echo IMAGES_DIR."/carrinho.png"; ?>" alt="foto de um carrinho"></button>
        <a id="minha conta" 
        href="/my-account"><img src="<?php echo IMAGES_DIR."/login.png"; ?>" alt="foto de login"></a>
    </div>
</header>
<div class="cart">

</div>

<div class="min-cart-invisible" id="miniCart">
    <?php wc_get_template( 'mincart.php' ); ?>
</div>

<script>
    let button_cart = document.querySelector("#abrir-carrinho")
    button_cart.addEventListener("click", () => openCart())

    let main = document.querySelector(".cart")

    let cart = document.createElement("a");
    let div_itens = document.createElement("div");
    let miniCart = document.querySelector("#miniCart")
    
    
    function openCart(){       
        let title = document.createElement("p");

        miniCart.classList.remove("min-cart-invisible")
        cart.classList.remove("zerar");
        div_itens.classList.remove("zerar");
        cart.classList.add("cart-st");
        div_itens.classList.add("itens-cart");
        title.classList.add("title");
        miniCart.classList.add("min-cart-visible")
        
        title.innerText = "Carrinho"
        
        div_itens.append(title);
        div_itens.innerHTML = '<br><br><div class="close"><a href="javascript:closeCart()">X</a></div>' + div_itens.innerHTML + '<div class="linha"></div> ' 
        
        main.append(cart);
        main.append(div_itens);
        div_itens.append(miniCart);
        
        cart.addEventListener("click", () => closeCart());
    }

    function closeCart(){
        cart.classList.remove("cart-st");
        div_itens.classList.remove("itens-cart");
        cart.classList.add("zerar");
        div_itens.classList.add("zerar");
        div_itens.innerText="";
        div_itens.innerHtml="";
    }
</script>
